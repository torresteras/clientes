<%-- 
    Document   : index
    Created on : 12-04-2020, 00:57:51
    Author     : alvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        
        <link href="css/bootstrap-reboot.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-grid.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-grid.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="validacion.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ingreso de clientes</h1><br>
        
        <div class="container-lg">

            <form onsubmit="return valida()" action="crear_cliente" method="POST">
                Identificacion:<br>
                <input id="identificacion" type="text" name="identificacion" value="" /><br>
                Nombres:<br>
                <input id="nombres" type="text" name="nombres" value="" /><br>
                Primer Apellido:<br>
                <input id="apellido1" type="text" name="apellido1" value="" /><br>
                Segundo Apellido:<br>
                <input id="apellido2" type="text" name="apellido2" value="" /><br>
                Teléfono:<br>
                <input id="telefono" type="text" name="telefono" value="" /><br>
                <br>
                <br>
                
                <input class="btn btn-info" type="submit" value="Enviar" />
                <a class="btn btn-info" href="index.jsp">Volver</a>
            </form>      
    </div>
</body>
</html>
