<%-- 
    Document   : eliminar_cliente
    Created on : 12-04-2020, 01:40:45
    Author     : alvaro
--%>

<%@page import="root.model.entities.MasterCliente"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    MasterCliente cliente = (MasterCliente) request.getAttribute("cliente");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar Cliente</title>
    </head>
    <body>
        <h1>Eliminar Cliente</h1>

        <form action="eliminar_cliente" method="POST">

            ID<br>
            <input type="text" name="id" value="<%= cliente.getCliIdentificacion()%>" />
            <input type="submit" value="Eliminar" />
        </form>

</html>
