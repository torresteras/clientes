<%-- 
    Document   : editar_cliente
    Created on : 12-04-2020, 17:57:21
    Author     : alvaro
--%>



<%@page import="root.model.entities.MasterCliente"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    MasterCliente cliente = (MasterCliente)request.getAttribute("cliente");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar cliente</title>
        <script type="text/javascript" src="validacion.js"></script>
    </head>
    <body>
        <h1>Editar cliente!</h1>

        <div class="container">
            <form onsubmit="return valida()" action="editar_cliente" method="POST">
                Identificacion:<br>
                <input id="identificacion"  readonly=""  name="identificacion" value="<%= cliente.getCliIdentificacion() %>" /> <br>
                Nombres:<br>
                <input id="nombres" type="text" name="nombres" value="<%= cliente.getCliNombres()%> " /><br>
                Primer Apellido:<br>
                <input id="apellido1" type="text" name="apellido1" value="<%= cliente.getCliApellido1()%>" /><br>
                Segundo Apellido:<br>
                <input id="apellido2" type="text" name="apellido2" value="<%= cliente.getCliApellido2()%>" /><br>
                Teléfono:<br>
                <input id="telefono" type="text" name="telefono" value="<%= cliente.getCliTelefono()%>  " />
                <br>
                <input class="btn btn-info" type="submit" value="Editar"  />
                <input class="btn btn-info" type="button" value="Volver" href="index.jsp">
            </form>
        </div>
    </body>
</html>   
