<%-- 
    Document   : lista
    Created on : 14-04-2020, 22:51:00
    Author     : alvaro
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.model.entities.MasterCliente"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<MasterCliente> clientes = (List<MasterCliente>) request.getAttribute("clientes");
    Iterator<MasterCliente> itClientes = clientes.iterator();
%>
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-reboot.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-grid.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-grid.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="validacion.js"></script>
        <title>Listado clientes</title>
    </head>
    <body class="row align-items-center justify-content-center">
        <form onsubmit="validacion()" name="form" action="controller_listado" method="post">

            <div class="row d-flex justify-content-center">
                <h1>Listado Clientes</h1>
                <table class="table table-bordered" border="1">
                    <thead style="background-color: windowframe">
                    <th class="text-center">Identificacion</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Apellido</th>
                    <th class="text-center">Apellido 2</th>
                    <th class="text-center" >teléfono</th>
                    <th class="text-center">Accion</th>

                    </thead>
                    <tbody>
                        <%
                            while (itClientes.hasNext()) {
                                MasterCliente cli = itClientes.next();
                        %>
                        <tr>
                            <td class="text-center"> <%= cli.getCliIdentificacion()%></td>
                            <td class="text-center"> <%= cli.getCliNombres()%> </td>
                            <td class="text-center"> <%= cli.getCliApellido1()%> </td>
                            <td class="text-center"> <%= cli.getCliApellido2()%>  </td>
                            <td class="text-center"> <%= cli.getCliTelefono() %> </td>
                    <br>

                            <td class="text-center">
                                <input id="seleccion" type="radio" name="seleccion"  value="<%= cli.getCliIdentificacion()%>">
                            </td>
                        </tr>
                        <%}%>
                    </tbody>
                </table>
                  
                <button type="submit" name="accion" value="editar" class="btn btn-primary">Editar </button>
                
                <button type="submit" name="accion" value="eliminar" class="btn btn-primary">Eliminar</button>
          
            </div>
            <a class="btn btn-info" href="crear_cliente.jsp">Ingresar cliente</a>
        </form >
    </body>
</html>
